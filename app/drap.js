var myDrag = function(document) {
    return function(scope, element, attr) {
        var dx,dy, startX = 0, startY = 0, x = 0, y = 0;
        element.css({
            position: 'relative',
            cursor:'pointer'
        });

        element.on('mousedown', function(event) {

            startX = event.pageX - x;
            startY = event.pageY - y;
                document.on('mousemove', mousemove);
                document.on('mouseup', mouseup);

        });


        function mousemove(event) {
            y = event.pageY - startY;
            x = event.pageX - startX;
            element.css({
                top: y + 'px',
                left: x + 'px'
            });
            element.one('click', function (event) {
                event.preventDefault();
            });
        }

        function mouseup() {
            document.off('mousemove', mousemove);
            document.off('mouseup', mouseup);
        }
    };
};




angular.module('myApp.drag',[])

.directive('drop', ['$document', myDrag]);
